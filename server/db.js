const mongoose = require("mongoose");
const dev = require("./dev.json");
const key = dev.key;
// console.log(key);
const connectDB = async () => {
  try {
    await mongoose.connect(key, {
      useNewUrlParser: true
    });
    console.log("mongoDB connected");
  } catch (err) {
    console.log(err.message);
    process.exit(1);
  }
};

module.exports = connectDB;
