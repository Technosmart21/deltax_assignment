const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let MovieSchema = new Schema({
  moviename: {
    type: String,
    required: true,
    unique: true,
    max: 100
  },
  year: {
    type: Number,
    required: true
  },
  plot: { type: String, required: true },
  casts: []
});

// Export the model
module.exports = mongoose.model("Movie", MovieSchema);
