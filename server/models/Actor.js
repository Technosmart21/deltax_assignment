const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let ActorSchema = new Schema({
  name: { type: String, required: true, unique: true },
  gender: { type: String, required: true },
  bio: { type: String, required: true },
  movies: [
    {
      movie_id: { type: String },
      movie_name: { type: String }
    }
  ]
});

// Export the model
module.exports = mongoose.model("Actor", ActorSchema);
