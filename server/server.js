const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");
const connectDB = require("./db");
connectDB();
const Movie = require("./models/Movies");
const Actor = require("./models/Actor");

const app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// MYSQL CONNECTION

app.get("/", (req, res) => {
  res.json({ message: "Home Route" });
});

app.get("/getMovies", (req, res) => {
  Movie.find({}, (err, movies) => {
    if (err) {
      res.json({ status: false, message: "Unable To Fetch Data" });
    }
    res.json({ status: true, movies: movies });
  });
});

app.get("/getCasts", (req, res) => {
  Actor.find({}, (err, casts) => {
    if (err) {
      res.json({ status: false, message: "Unable To Fetch Data" });
    }
    res.json({ status: true, casts: casts });
  });
});

app.post("/addMovie", (req, res) => {
  let movie = new Movie(req.body);

  movie.save((err, movie) => {
    if (err) {
      res.json({ status: false, message: "Error in Inserting" });
    } else {
      res
        .status(201)
        .json({ status: true, message: "Movie Inserted Successfully" });
    }
  });
});

app.post("/addCast", (req, res) => {
  let cast = new Actor(req.body);

  cast.save((err, cast) => {
    if (err) {
      res.json({ status: false, message: "Error" });
    } else {
      res
        .status(201)
        .json({ status: true, message: "Cast Inserted Successfully" });
    }
  });
});

app.put("/updateMovie", (req, res) => {
  console.log(req.body.movie_id);
  Movie.findByIdAndUpdate(
    req.body.movie_id,
    {
      $set: {
        moviename: req.body.moviename,
        year: req.body.year,
        plot: req.body.plot
      }
    },
    (err, res) => {
      if (err) {
        res.json({ status: false, message: "Error in Update" });
      } else {
        res
          .status(201)
          .json({ status: true, message: "Movie Updated Successfully" });
      }
    }
  );
});

app.delete("/deleteMovie/:id", (req, res) => {
  Movie.remove({ _id: req.params.id }, (err, movie) => {
    if (err) {
      res.status(404).json({ status: false, message: "Error in Delete" });
    }
    res
      .status(200)
      .json({ status: true, message: "Movie successfully deleted" });
  });
});

const port = process.env.PORT || 4000;

app.listen(port, function() {
  console.log("Server Running On port 4000");
});
