import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ApiService } from "../../services/api.service";

@Component({
  selector: "app-edit-movie",
  templateUrl: "./edit-movie.component.html",
  styleUrls: ["./edit-movie.component.sass"]
})
export class EditMovieComponent implements OnInit {
  movie_id;
  movieForm: FormGroup;
  message: String = "";
  showError: boolean = false;
  showSuccess: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private Api: ApiService
  ) {
    this.movie_id = this.route.snapshot.params.id;

    this.movieForm = this.fb.group({
      moviename: ["", Validators.required],
      year: ["", [Validators.required]],
      plot: ["", [Validators.required]]
    });
  }

  onEditMovie() {
    if (this.movieForm.dirty && this.movieForm.valid) {
      let movie = {
        movie_id: this.movie_id,
        moviename: this.movieForm.value.moviename,
        year: this.movieForm.value.year,
        plot: this.movieForm.value.plot
      };
      let response;
      this.Api.updateMovie(movie).subscribe(res => {
        response = res;
        console.log(res);
        this.message = response.message;
        if (response.status) {
          this.showSuccess = true;
          this.showError = false;
        } else {
          this.showSuccess = false;
          this.showError = true;
        }
      });
    }
  }
  ngOnInit() {}
}
