import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ApiService } from "../../services/api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-movie",
  templateUrl: "./add-movie.component.html",
  styleUrls: ["./add-movie.component.sass"]
})
export class AddMovieComponent implements OnInit {
  movieForm: FormGroup;
  castForm: FormGroup;
  casts: any[];
  selectedCasts: any[] = [];
  castsData;
  message: String = "";
  showError: boolean = false;
  showSuccess: boolean = false;

  @ViewChild("myModal") myModal;
  display: string;
  constructor(
    private fb: FormBuilder,
    private Api: ApiService,
    private router: Router
  ) {
    let d = new Date();
    let year = d.getFullYear();
    this.display = "none";

    this.movieForm = this.fb.group({
      moviename: ["", Validators.required],
      year: [year, [Validators.required]],
      plot: ["", [Validators.required]]
    });

    this.castForm = this.fb.group({
      name: ["", Validators.required],
      gender: ["Male", [Validators.required]],
      bio: ["", [Validators.required]]
    });

    this.getAllCasts();
  }

  getCastDetail(name) {
    this.selectedCasts.push(name);
  }

  openModal() {
    this.display = "block";
  }

  onCloseHandled() {
    this.display = "none";
  }

  onAddMovie() {
    if (this.movieForm.dirty && this.movieForm.valid) {
      let movie = {
        moviename: this.movieForm.value.moviename,
        year: this.movieForm.value.year,
        plot: this.movieForm.value.plot,
        casts: this.selectedCasts
      };

      let response;
      this.Api.postMovie(movie).subscribe(res => {
        response = res;
        this.message = response.message;
        if (response.status) {
          this.showSuccess = true;
          this.showError = false;

          setTimeout(() => {
            this.router.navigate(["/"]);
          }, 2000);
        } else {
          this.showSuccess = false;
          this.showError = true;
        }
        this.selectedCasts = [];
      });
    }
  }

  onAddCast() {
    if (this.castForm.dirty && this.castForm.valid) {
      let cast = {
        name: this.castForm.value.name,
        gender: this.castForm.value.gender,
        bio: this.castForm.value.bio
      };
      let response;
      this.Api.addCast(cast).subscribe(res => {
        response = res;
        if (response.status) {
          this.getAllCasts();
          this.onCloseHandled();
        }
      });
    }
  }

  getAllCasts() {
    this.Api.getCasts().subscribe(res => {
      this.castsData = res;

      if (this.castsData.status) {
        this.casts = this.castsData.casts;
      }
    });
  }

  ngOnInit() {}
}
