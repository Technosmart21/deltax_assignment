import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api.service";

@Component({
  selector: "app-movies",
  templateUrl: "./movies.component.html",
  styleUrls: ["./movies.component.sass"]
})
export class MoviesComponent implements OnInit {
  movies: any[];
  movies_data;
  year;
  constructor(private api: ApiService) {
    let d = new Date();
    this.year = d.getFullYear();
  }

  ngOnInit() {
    this.getMovies();
  }

  getMovies() {
    this.api.getMovies().subscribe((res: any[]) => {
      this.movies_data = res;
      this.movies = this.movies_data.movies;
    });
  }

  onDeleteMovie(id) {
    // console.log(id);
    if (confirm("Do You Delete this Movie!")) {
      let response;
      this.api.deleteMovie(id).subscribe(res => {
        response = res;
        if (response.status) {
          alert(response.message);
        }
        this.getMovies();
      });
    }
  }
}
