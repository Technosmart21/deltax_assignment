import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { CelebsComponent } from "./components/celebs/celebs.component";
import { MoviesComponent } from "./components/movies/movies.component";
import { ApiService } from "./services/api.service";
import { EditMovieComponent } from "./components/edit-movie/edit-movie.component";
import { AddMovieComponent } from "./components/add-movie/add-movie.component";
import { ModalComponent } from './components/modal/modal.component';

const route: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full"
  },
  {
    path: "addMovie",
    component: AddMovieComponent
  },
  {
    path: "home",
    component: HomeComponent,
    children: [
      { path: "", component: MoviesComponent },
      { path: "edit/:id", component: EditMovieComponent }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    CelebsComponent,
    MoviesComponent,
    EditMovieComponent,
    AddMovieComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(route)
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
