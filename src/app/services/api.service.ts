import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//import "rxjs/add/operator/map";
// import "rxjs/add/operator/map";
import { Router } from "@angular/router";

@Injectable()
export class ApiService {
  constructor(private http: HttpClient, private router: Router) {}

  getMovies() {
    return this.http.get("http://localhost:4000/getMovies");
  }

  postMovie(movie) {
    return this.http.post("http://localhost:4000/addMovie", movie);
  }

  addCast(cast) {
    return this.http.post("http://localhost:4000/addCast", cast);
  }

  getCasts() {
    return this.http.get("http://localhost:4000/getCasts");
  }

  updateMovie(newMovie) {
    return this.http.put("http://localhost:4000/updateMovie", newMovie);
  }

  deleteMovie(movie_id) {
    return this.http.delete("http://localhost:4000/deleteMovie/" + movie_id);
  }
}
